class Apps {
  Apps(String aAppName, String aSector, String aDeveloper, int aYear) {
    this.appName = aAppName;
    this.sector = aSector;
    this.developer = aDeveloper;
    this.year = aYear;
  }

  String? appName;
  String? sector;
  String? developer;
  int? year;
}

void main() {
  Apps app2012 = Apps('FNB Banking App', 'Consumer', 'FNB', 2012);
  Apps app2013 = Apps('SnapScan', 'Consumer', 'Gerrit Greet', 2013);
  Apps app2014 = Apps('Live Inspect', 'Consumer', 'Lightstone', 2014);
  Apps app2015 = Apps('WumDrop', 'Enterprise', 'Simon Hartley', 2015);
  Apps app2016 = Apps('Domestly', 'Consumer', 'Berno Potgieter', 2016);
  Apps app2017 = Apps('Shyft', 'Finance', 'Breth Patrontasch', 2017);
  Apps app2018 = Apps('Khula', 'Consumer', 'Karidas Tshintsholo', 2018);
  Apps app2019 = Apps('Naked Insurance', 'Finance', 'Sumarie Greybe', 2019);
  Apps app2020 = Apps('EasyEquity', 'Investment', 'Charles Savage', 2020);
  Apps app2021 = Apps('Ambani', 'Education', 'Mukundi Lambani', 2021);

  print(app2021.appName);
}
